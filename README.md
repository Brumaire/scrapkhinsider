# Get musics from Khinsider album page

## Intro

This ten minutes project is a python script that allows scrapping games
soundtracks
from [khinsider.com](https://downloads.khinsider.com) (no account required).

## Motivation

- I love listening to some games soundtracks, but these are usually not commercialy nor
freely available at all.

- Most game soundtracks lovers will usually rely on ripping games to find the tracks they
want. 

- Khinsider is a website that archive such rips. 

- Unfortunately, to download full soundtracks at once, Khinsider will requires you to create an account.

- Account requirement is invasive regarding privacy. Having lots of accounts
  everywhere is a poor practice regarding security as it multiplies the attack
  surface.

## Disclaimer

Through I **do** support piracy regardless of motivations involved,
I **do not** support parasitism. **Please support game (and soundtracks) makers
by buying their games AND please donate to khinsider** !

## Dependencies

Python 3.X
Beautiful Soup 4

You may install BS4 with pip or any other packaging mean.

## Usage

```bash
python [PATH_TO]/scrapKhinsider.py dest_dir url [optional_links_regex_filter]
```

One may set `KHINSIDER_NO_WRITE` env variable to `TRUE` to disable writing
to disk (useful for tests on the scrapping parts)

## Example

Assume you want to DL the OST of Just Shapes & Beats.

Find the album page on
[downloads.khinsider.com](https://downloads.khinsider.com) :

```
https://downloads.khinsider.com/game-soundtracks/album/just-shapes-beats-2018
```

Then, run the script :

```bash
python scrapKhinsider.py /home/user/osts/JustShapesAndBeats https://downloads.khinsider.com/game-soundtracks/album/just-shapes-beats-2018
```

## Licensing

Do-what-you-want license

## Contributing

Welcomed
