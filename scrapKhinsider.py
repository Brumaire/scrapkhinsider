#!/usr/bin/env python

import sys
import re
import urllib.parse
from urllib.parse import unquote
from bs4 import BeautifulSoup
import requests
import os
from os.path import basename
from os.path import abspath

### HELPER FUNCTIONS ###

#Handle inner links
def handleInnerLink(href2):
  # Get an absolute link from relative
  link2 = urllib.parse.urljoin(DATA_PAGE, href2)
  print(f"Found Inner link : {link}")
  # Build path to write the file to
  path = os.path.join(DEST_PATH,basename(unquote(link2)))
  print(f"Output path : {path}")
  # Fetch audio file and write it
  if not (os.environ["KHINSIDER_NO_WRITE"] == "TRUE"):
    with open(path, "wb") as f:
      response2 = requests.get(link2)
      f.write(response2.content)
      print(f"Wrote {path}")

#HTML A Elements filter function 
def allSongsRefFilter(href):
    return href and re.compile(".mp3").search(href) and re.compile(FILTER).search(href)

### GLOBALS ###

#Get args
if len(sys.argv) >= 3:
  DATA_PAGE = sys.argv[2]
  DEST_PATH = abspath(sys.argv[1])
else:
  print(f"Usage : {sys.argv[0]} path_to_dest url [filter]")
  exit(1)
#Optional filter
if len(sys.argv) >= 4:
  FILTER = sys.argv[3]
else:
  FILTER = ""

### MAIN ###
  
print(f"Destination path is {DEST_PATH}")
#Enable sessions in case we need cookies to be allowed to the DL page
client = requests.Session()
#HTTP Request of the page
dataResponse = client.get(DATA_PAGE)
#Explore the DOM with soup
soup = BeautifulSoup(dataResponse.content, 'html.parser')
#Found all links matching our criterions
matchingElements = soup.find_all('a', href=allSongsRefFilter)
#Get all uniques URL from HREF attributes 
links = map(lambda a : a["href"], matchingElements)
links = list(set(links))
#We must scrap another page for each link
for href in links:
  #Get absolute link
  link = urllib.parse.urljoin(DATA_PAGE, href)
  print(f"Found link : {link}") 
  print(f"Recursive exploration of link...") 
  #HTTP Get the second page
  dataResponse2 = client.get(link)
  #Let's scrap it
  soup2 = BeautifulSoup(dataResponse2.content, 'html.parser')
  #We use the same criterion for the search
  matchingElements2 = soup2.find_all('a', href=allSongsRefFilter)
  #Same as prev, but this time we should have only one link
  links2 = map(lambda a: a["href"], matchingElements2) 
  #Call handler function to handle the DL
  for href2 in links2:
    handleInnerLink(href2)
